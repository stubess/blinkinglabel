public class BlinkingLabel : UILabel {
    public func startBlinking() {
        let options : UIView.AnimationOptions = [.repeat, .autoreverse]
        UIView.animate(withDuration: 0.50, delay:0.0, options:options, animations: {
            self.alpha = 0
        }, completion: nil)
    }
    
    public func stopBlinking() {
        alpha = 1
        layer.removeAllAnimations()
    }
}

